+++
title="Previous versions"
+++

* [libxc-1.0.0.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/1.0.0/libxc-1.0.0.tar.bz2)
* [libxc-1.1.0.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/1.1.0/libxc-1.1.0.tar.bz2)
* [libxc-1.2.0.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/1.2.0/libxc-1.2.0.tar.bz2)
* [libxc-2.0.0.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/2.0.0/libxc-2.0.0.tar.bz2)
* [libxc-2.0.1.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/2.0.1/libxc-2.0.1.tar.bz2)
* [libxc-2.0.2.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/2.0.2/libxc-2.0.2.tar.bz2)
* [libxc-2.0.3.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/2.0.3/libxc-2.0.3.tar.bz2)
* [libxc-2.1.0.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/2.1.0/libxc-2.1.0.tar.bz2)
* [libxc-2.1.1.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/2.1.1/libxc-2.1.1.tar.bz2)
* [libxc-2.1.2.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/2.1.2/libxc-2.1.2.tar.bz2)
* [libxc-2.1.3.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/2.1.3/libxc-2.1.3.tar.bz2)
* [libxc-2.2.0.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/2.2.0/libxc-2.2.0.tar.bz2)
* [libxc-2.2.1.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/2.2.1/libxc-2.2.1.tar.bz2)
* [libxc-2.2.2.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/2.2.2/libxc-2.2.2.tar.bz2)
* [libxc-2.2.3.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/2.2.3/libxc-2.2.3.tar.bz2)
* [libxc-3.0.0.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/3.0.0/libxc-3.0.0.tar.bz2)
* [libxc-3.0.1.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/3.0.1/libxc-3.0.1.tar.bz2)
* [libxc-4.0.0.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/4.0.0/libxc-4.0.0.tar.bz2)
* [libxc-4.0.1.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/4.0.1/libxc-4.0.1.tar.bz2)
* [libxc-4.0.2.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/4.0.2/libxc-4.0.2.tar.bz2)
* [libxc-4.0.3.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/4.0.3/libxc-4.0.3.tar.bz2)
* [libxc-4.0.4.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/4.0.4/libxc-4.0.4.tar.bz2)
* [libxc-4.0.5.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/4.0.5/libxc-4.0.5.tar.bz2)
* [libxc-4.1.0.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/4.1.0/libxc-4.1.0.tar.bz2)
* [libxc-4.1.1.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/4.1.1/libxc-4.1.1.tar.bz2)
* [libxc-4.2.0.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/4.2.0/libxc-4.2.0.tar.bz2)
* [libxc-4.2.1.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/4.2.1/libxc-4.2.1.tar.bz2)
* [libxc-4.2.2.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/4.2.2/libxc-4.2.2.tar.bz2)
* [libxc-4.2.3.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/4.2.3/libxc-4.2.3.tar.bz2)
* [libxc-4.3.0.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/4.3.0/libxc-4.3.0.tar.bz2)
* [libxc-4.3.1.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/4.3.1/libxc-4.3.1.tar.bz2)
* [libxc-4.3.2.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/4.3.2/libxc-4.3.2.tar.bz2)
* [libxc-4.3.3.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/4.3.3/libxc-4.3.3.tar.bz2)
* [libxc-4.3.4.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/4.3.4/libxc-4.3.4.tar.bz2)
* [libxc-5.0.0.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/5.0.0/libxc-5.0.0.tar.bz2)
* [libxc-5.1.0.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/5.1.0/libxc-5.1.0.tar.bz2)
* [libxc-5.1.1.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/5.1.1/libxc-5.1.1.tar.bz2)
* [libxc-5.1.2.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/5.1.2/libxc-5.1.2.tar.bz2)
* [libxc-5.1.3.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/5.1.3/libxc-5.1.3.tar.bz2)
* [libxc-5.1.4.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/5.1.4/libxc-5.1.4.tar.bz2)
* [libxc-5.1.5.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/5.1.5/libxc-5.1.5.tar.bz2)
* [libxc-5.1.6.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/5.1.6/libxc-5.1.6.tar.bz2)
* [libxc-5.1.7.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/5.1.7/libxc-5.1.7.tar.bz2)
* [libxc-5.2.0.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/5.2.0/libxc-5.2.0.tar.bz2)
* [libxc-5.2.1.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/5.2.1/libxc-5.2.1.tar.bz2)
* [libxc-5.2.2.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/5.2.2/libxc-5.2.2.tar.bz2)
* [libxc-5.2.3.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/5.2.3/libxc-5.2.3.tar.bz2)
* [libxc-6.0.0.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/6.0.0/libxc-6.0.0.tar.bz2)
* [libxc-6.1.0.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/6.1.0/libxc-6.1.0.tar.bz2)
* [libxc-6.2.0.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/6.2.0/libxc-6.2.0.tar.bz2)
* [libxc-6.2.1.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/6.2.1/libxc-6.2.1.tar.bz2)
* [libxc-6.2.2.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/6.2.2/libxc-6.2.2.tar.bz2)
* [libxc-7.0.0.tar.bz2](https://gitlab.com/libxc/libxc/-/archive/7.0.0/libxc-7.0.0.tar.bz2)
